public class Main {
    public static void main(String[] args) {

        //creation d'un client pro et d'un particulier
        ClientPro clientPro = new ClientPro("EKW02062023", "135792468", "Entreprise", 2000000.0);
        ClientPro clientPro2 = new ClientPro("EKW03062023", "135792460", "Entreprise 2", 100000.0);
        Particulier particulier = new Particulier("EKW04062023", "M.", "SOUMARE", "Kandioura");

        //creation de deux factures
        Facture facture1 = new Facture(clientPro, "Electricite", 121009.0);
        Facture facture3 = new Facture(clientPro, "Electricite", 10380.0);
        Facture facture5 = new Facture(particulier, "Gaz", 799.0);

        //calcul du montant des deux factures
        double montant1 = facture1.calculerMontant();
        double montant3 = facture3.calculerMontant();
        double montant5 = facture5.calculerMontant();

        //affichage des montants des deux facturesgetReference
        System.out.println("Voici le montant de la facture du Client Pro " + clientPro.getReference() + " : " + montant1 + " €");
        System.out.println("Voici le montant de la facture du Client Pro " + clientPro2.getReference() + " : " + montant3 + " €");
        System.out.println("Vpici le montant de la facture de " + particulier.identite() + " : " + montant5 + " €");

    }
}
