class ClientPro extends Client {
    private String siret;
    private String raisonSociale;
    private double ca;

    public ClientPro(String reference, String siret, String raisonSociale, double ca) { //constructeur de la classe ClientPro
        super(reference);
        this.siret = siret;
        this.raisonSociale = raisonSociale;
        this.ca = ca;
    }

    public double getCA() { //getter du CA
        return ca;
    }
}