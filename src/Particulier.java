class Particulier extends Client { //cette classe herite de la classe Client
    private String civilite;
    private String nom;
    private String prenom;

    public Particulier(String reference, String civilite, String nom, String prenom) { //constructeur de la classe Particulier
        super(reference);
        this.civilite = civilite;
        this.nom = nom;
        this.prenom = prenom;
    }

    public String identite() { //cette methode sert a retourner l'identite du client
        return "" + civilite + "" + nom + " " + prenom;
    }
}