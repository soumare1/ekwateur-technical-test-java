class Facture {
    private Client client; // ClientPro ou Particulier
    private String energie; // "Electricite" ou "Gaz"
    private double consommation; // en kWh

    public Facture(Client client, String energie, double consommation) {
        this.client = client;
        this.energie = energie;
        this.consommation = consommation;
    }

    public double calculerMontant() { //cette methode sert a calculer le montant de la facture
        double prixKWh;

        if (client instanceof Particulier) { //si le client est un particulier
            prixKWh = energie.equals("Electricite") ? 0.121 : 0.115;
        } else if (client instanceof ClientPro) { //si le client est un client pro
            double ca = ((ClientPro) client).getCA();
            if (ca > 1000000) { //si le CA est superieur a 1000000
                prixKWh = energie.equals("Electricite") ? 0.114 : 0.111;
            } else { //si le CA est inferieur a 1000000
                prixKWh = energie.equals("Electricite") ? 0.118 : 0.113;
            }
        } else { //si le client n'est ni un particulier ni un client pro
            prixKWh = 0.0;
        }

        return consommation * prixKWh;
    }
}