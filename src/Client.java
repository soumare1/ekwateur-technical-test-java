class Client {
    private String reference;

    public Client(String reference) { //constructeur de la classe Client
        this.reference = reference;
    }

    public String getReference() { //getter de la reference
        return reference;
    }
}